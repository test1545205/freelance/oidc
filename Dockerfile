FROM openjdk:19-jdk-alpine
ARG JAR_FILE=target/spring-boot-auth-server.jar
COPY ${JAR_FILE} application.jar
ENTRYPOINT ["java", "-jar", "application.jar"]
EXPOSE 8081