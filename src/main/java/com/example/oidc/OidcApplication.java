package com.example.oidc;

import com.example.oidc.entity.RoleEntity;
import com.example.oidc.entity.UserEntity;
import com.example.oidc.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@SpringBootApplication
public class OidcApplication {

	public static void main(String[] args) {
		SpringApplication.run(OidcApplication.class, args);
	}

	@Bean
	CommandLineRunner run(UserService userService){
		return args -> {

			RoleEntity adminRole = userService.saveRole(new RoleEntity(null, "ADMIN"));
			RoleEntity userMakerRole = userService.saveRole(new RoleEntity(null, "MAKER"));
			RoleEntity userCheckerRole = userService.saveRole(new RoleEntity(null, "CHECKER"));
			RoleEntity userApproverRole = userService.saveRole(new RoleEntity(null, "APPROVER"));

			Collection<RoleEntity> roles = new ArrayList<>();
			roles.add(adminRole);

			Collection<RoleEntity> roles2 = new ArrayList<>();
			roles2.add(userMakerRole);

			Collection<RoleEntity> roles3 = new ArrayList<>();
			roles3.add(userCheckerRole);

			Collection<RoleEntity> roles4 = new ArrayList<>();
			roles4.add(userApproverRole);

			userService.saveUser(new UserEntity(null, "admin", "admin", "admin@gmail.com", "password", roles));
			userService.saveUser(new UserEntity(null, "maker", "maker", "maker@gmail.com", "password", roles2));
			userService.saveUser(new UserEntity(null, "checker", "checker", "cheker@gmail.com", "password", roles3));
			userService.saveUser(new UserEntity(null, "approver", "approver", "approver@gmail.com", "password", roles4));

		};
	}

}
