package com.example.oidc.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.management.relation.Role;
import java.util.List;

public class UserDto {

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Save{
        private String firstName;
        private String lastName;
        private String email;
        private String password;
        private List<Role> roles;
    }



}
