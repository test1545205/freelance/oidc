package com.example.oidc.repository;

import com.example.oidc.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRespository extends JpaRepository<RoleEntity, Long> {
    RoleEntity findByName(String name);
}
