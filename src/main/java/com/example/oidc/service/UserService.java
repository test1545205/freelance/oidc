package com.example.oidc.service;

import com.example.oidc.entity.RoleEntity;
import com.example.oidc.entity.UserEntity;
import com.example.oidc.repository.RoleRespository;
import com.example.oidc.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {

    private final UserRepository userRepository;
    private final RoleRespository roleRespository;
    private final PasswordEncoder passwordEncoder;

    public UserEntity saveUser(UserEntity user){
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    public RoleEntity saveRole(RoleEntity role){
        return roleRespository.save(role);
    }

    public UserEntity getUser(String email){
        return userRepository.findByEmail(email);
    }

    public void addRoleToUser(String email, String roleName){
        UserEntity user = userRepository.findByEmail(email);
        RoleEntity role = roleRespository.findByName(roleName);
        user.getRoles().add(role);
    }

}
